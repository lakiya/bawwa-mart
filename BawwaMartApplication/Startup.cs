﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BawwaMartApplication.Startup))]
namespace BawwaMartApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
