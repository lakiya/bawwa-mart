﻿using BawwaMartApplication.Models;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace BawwaMartApplication.Controllers
{
    public class HomeController : Controller
    {
        private BawwaMartEntities db = new BawwaMartEntities();
        public ActionResult Index()
        {
            var currentUser = User.Identity.GetUserId();
            User BawwaMartUser = db.Users.Where(x => x.AspNetUserId.Equals(currentUser)).FirstOrDefault();
            ViewBag.BawwaMartUser = BawwaMartUser;

            ViewBag.Pets = db.Pets.OrderByDescending(x => x.CreatedDate).Take(3).ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}