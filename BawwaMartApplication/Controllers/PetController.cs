﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BawwaMartApplication.Models;
using Microsoft.AspNet.Identity;

namespace BawwaMartApplication.Controllers
{
    public class PetController : Controller
    {
        private BawwaMartEntities db = new BawwaMartEntities();

        // GET: Pet
        public ActionResult Index()
        {
            List<PetCategory> PetCategoryList = db.PetCategories.ToList();
            ViewBag.PetCategoryList = PetCategoryList;
            return View(db.Pets.ToList());
        }

        public ActionResult Mine()
        {
            String userId = User.Identity.GetUserId();
            return View(db.Pets.Where(x => x.Sales.FirstOrDefault().AspNetUser.Id == userId));
        }

        public ActionResult Search(string Keyword, int PetCategoryId)
        {
            List<PetCategory> PetCategoryList = db.PetCategories.ToList();
            ViewBag.PetCategoryList = PetCategoryList;
            List<Pet> PetList;
            if (Keyword != null && Keyword != "")
            {
                PetList = db.Pets.Where(x => ((x.Description.Contains(Keyword) || x.Sales.FirstOrDefault().Title.Contains(Keyword)) && x.PetCategoryId == PetCategoryId)).ToList();
            }
            else
            {
                PetList = db.Pets.Where(x => x.PetCategoryId == PetCategoryId).ToList();
            }
            return View("Index", PetList);
        }

        // GET: Pet/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pet pet = db.Pets.Find(id);
            if (pet == null)
            {
                return HttpNotFound();
            }
            var PublishedUser = pet.Sales.FirstOrDefault().PublishedUserId;
            User BawwaMartUser = db.Users.Where(x => x.AspNetUserId == PublishedUser).FirstOrDefault();
            ViewBag.FullName = BawwaMartUser.FirstName + " " + BawwaMartUser.LastName;
            ViewBag.Phone = BawwaMartUser.AspNetUser.PhoneNumber;
            ViewBag.Email = BawwaMartUser.AspNetUser.Email;
            return View(pet);
        }

        // GET: Pet/Create
        [Authorize]
        public ActionResult Create()
        {
            List<PetCategory> PetCategoryList = db.PetCategories.ToList();
            ViewBag.PetCategoryList = PetCategoryList;
            return View();
        }

        // POST: Pet/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "PetCategoryId, District, Title, Price, Description, ImageFile1, ImageFile2, ImageFile3")] PetSaleModel petSaleModel)
        {
            Pet pet = new Pet();
            if (ModelState.IsValid)
            {
                pet.CreatedBy = System.Web.HttpContext.Current.User.Identity.Name;
                pet.CreatedDate = DateTime.Now;
                pet.LastModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
                pet.LastModifiedDate = DateTime.Now;
                pet.Description = petSaleModel.Description;
                pet.PetCategoryId = petSaleModel.PetCategoryId;

                db.Pets.Add(pet);
                db.SaveChanges();

                if (petSaleModel.ImageFile1 != null)
                {
                    SaveImage(petSaleModel.ImageFile1, pet, "1");
                }
                if (petSaleModel.ImageFile2 != null)
                {
                    SaveImage(petSaleModel.ImageFile2, pet, "2");
                }
                if (petSaleModel.ImageFile3 != null)
                {
                    SaveImage(petSaleModel.ImageFile3, pet, "3");
                }

                if (petSaleModel.ImageFile1 == null && petSaleModel.ImageFile2 == null && petSaleModel.ImageFile3 == null)
                {
                    PetImage petImage = new PetImage
                    {
                        ImageUrl = "~/Resources/default.jpg"
                    };

                    petImage.PetId = pet.Id;
                    db.PetImages.Add(petImage);
                    db.SaveChanges();
                }
                Sale sale = new Sale
                {
                    PetId = pet.Id,
                    Price = petSaleModel.Price,
                    Title = petSaleModel.Title,
                    District = petSaleModel.District,
                    LastModifiedDate = DateTime.Now,
                    PublishedUserId = User.Identity.GetUserId()
                };
                db.Sales.Add(sale);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            List<PetCategory> PetCategoryList = db.PetCategories.ToList();
            ViewBag.PetCategoryList = PetCategoryList;
            return View(petSaleModel);
        }

        private void SaveImage(HttpPostedFileBase ImageFile, Pet pet, string fileNameParm)
        {
            string FileName = Path.GetFileNameWithoutExtension(ImageFile.FileName);
            string FileExtension = Path.GetExtension(ImageFile.FileName);
            FileName = FileName.Trim() + "-" + System.Web.HttpContext.Current.User.Identity.Name + "-" + DateTime.Now.ToString("yyyyMMdd") + "-" + fileNameParm + FileExtension;

            PetImage petImage = new PetImage
            {
                ImageUrl = "~/Resources/" + FileName
            };

            string finalImagePath = Path.Combine(Server.MapPath("~/Resources/"), FileName);

            //To copy and save file into server.  
            ImageFile.SaveAs(finalImagePath);

            petImage.PetId = pet.Id;
            db.PetImages.Add(petImage);
            db.SaveChanges();
        }

        // GET: Pet/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            List<PetCategory> petCategoryList = db.PetCategories.ToList();
            ViewBag.petCategoryList = petCategoryList;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pet pet = db.Pets.Find(id);
            if (pet == null)
            {
                return HttpNotFound();
            }

            PetSaleModel petSaleModal = new PetSaleModel();
            petSaleModal.Id = pet.Id;
            petSaleModal.Description = pet.Description;
            petSaleModal.District = pet.Sales.FirstOrDefault().District;
            petSaleModal.PetCategoryId = pet.PetCategoryId;
            petSaleModal.Price = pet.Sales.FirstOrDefault().Price;
            petSaleModal.Title = pet.Sales.FirstOrDefault().Title;
            return View(petSaleModal);
        }

        // POST: Pet/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Id, PetCategoryId, District, Title, Price, Description")] PetSaleModel petSaleModel)
        {
            Pet pet = db.Pets.Find(petSaleModel.Id);
            pet.LastModifiedDate = DateTime.Now;
            pet.PetCategoryId = petSaleModel.PetCategoryId;
            pet.Description = petSaleModel.Description;
            pet.Sales.FirstOrDefault().District = petSaleModel.District;
            pet.Sales.FirstOrDefault().Title = petSaleModel.Title;
            pet.Sales.FirstOrDefault().Price = petSaleModel.Price;

            pet.LastModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
            pet.LastModifiedDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Entry(pet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(petSaleModel);
        }

        // GET: Pet/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pet pet = db.Pets.Find(id);
            if (pet == null)
            {
                return HttpNotFound();
            }
            return View(pet);
        }

        // POST: Pet/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            List<PetImage> PetImageList = db.PetImages.Where(x => x.PetId == id).ToList();
            foreach (PetImage petImage in PetImageList)
            {
                db.PetImages.Remove(petImage);
            }

            List<Sale> SaleList = db.Sales.Where(x => x.PetId == id).ToList();
            foreach (Sale sale in SaleList)
            {
                db.Sales.Remove(sale);
            }

            Pet pet = db.Pets.Find(id);
            db.Pets.Remove(pet);

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
