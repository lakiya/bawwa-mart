﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using BawwaMartApplication.Models;
using Microsoft.AspNet.Identity;

namespace BawwaMartApplication.Controllers
{
    public class EventsController : Controller
    {
        private BawwaMartEntities db = new BawwaMartEntities();

        // GET: Events
        public ActionResult Index()
        {
            var events = db.Events;
            return View(events.ToList());
        }

        // GET: Events/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title,Description,ImageFile")] EventModel EventModel)
        {
            if (ModelState.IsValid)
            {
                Event @event = new Event();
                @event.Title = EventModel.Title;
                @event.Description = EventModel.Description;
                @event.CreatedUserId = User.Identity.GetUserId();


                string FileName = Path.GetFileNameWithoutExtension(EventModel.ImageFile.FileName);
                string FileExtension = Path.GetExtension(EventModel.ImageFile.FileName);
                FileName = FileName.Trim() + "-" + System.Web.HttpContext.Current.User.Identity.Name + "-" + DateTime.Now.ToString("yyyyMMdd") + "-event" + FileExtension;
                string finalImagePath = Path.Combine(Server.MapPath("~/Resources/"), FileName);

                //To copy and save file into server.  
                EventModel.ImageFile.SaveAs(finalImagePath);

                @event.Image = "~/Resources/" + FileName;

                db.Events.Add(@event);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(EventModel);
        }

        // GET: Events/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Events.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // POST: Events/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Description")] Event @event)
        {
            if (ModelState.IsValid)
            {
                Event dbEvent = db.Events.Find(@event.Id);
                dbEvent.Title = @event.Title;
                dbEvent.Description = @event.Description;
                db.Entry(dbEvent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(@event);
        }

        // GET: Events/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Events.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Event @event = db.Events.Find(id);
            db.Events.Remove(@event);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
