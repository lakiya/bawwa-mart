﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using BawwaMartApplication.Models;

namespace BawwaMartApplication.Controllers
{
    public class UsersController : Controller
    {
        private BawwaMartEntities db = new BawwaMartEntities();

        // GET: Users
        public ActionResult Index()
        {
            var users = db.Users.Include(u => u.AspNetUser);
            return View(users.ToList());
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
