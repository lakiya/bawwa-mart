﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using BawwaMartApplication.Models;

namespace BawwaMartApplication.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PetCategoryController : Controller
    {
        private BawwaMartEntities db = new BawwaMartEntities();

        // GET: PetCategory
        public ActionResult Index()
        {
            return View(db.PetCategories.ToList());
        }

        // GET: PetCategory/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PetCategory petCategory = db.PetCategories.Find(id);
            if (petCategory == null)
            {
                return HttpNotFound();
            }
            return View(petCategory);
        }

        // GET: PetCategory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PetCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name")] PetCategory petCategory)
        {
            if (ModelState.IsValid)
            {
                petCategory.CreatedBy = System.Web.HttpContext.Current.User.Identity.Name;
                petCategory.CreatedDate = DateTime.Now;
                petCategory.LastModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
                petCategory.LastModifiedDate = DateTime.Now;
                db.PetCategories.Add(petCategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(petCategory);
        }

        // GET: PetCategory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PetCategory petCategory = db.PetCategories.Find(id);
            if (petCategory == null)
            {
                return HttpNotFound();
            }
            return View(petCategory);
        }

        // POST: PetCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,status")] PetCategory petCategory)
        {
            if (ModelState.IsValid)
            {
                petCategory.LastModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
                petCategory.LastModifiedDate = DateTime.Now;
                db.Entry(petCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(petCategory);
        }

        // GET: PetCategory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PetCategory petCategory = db.PetCategories.Find(id);
            if (petCategory == null)
            {
                return HttpNotFound();
            }
            return View(petCategory);
        }

        // POST: PetCategory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PetCategory petCategory = db.PetCategories.Find(id);
            db.PetCategories.Remove(petCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
