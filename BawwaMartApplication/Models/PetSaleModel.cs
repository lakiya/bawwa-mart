﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace BawwaMartApplication.Models
{
    public class PetSaleModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int PetCategoryId { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public String Title { get; set; }
        [Required]
        public Decimal Price { get; set; }
        [Required]
        public String District { get; set; }
        public HttpPostedFileBase ImageFile1 { get; set; }
        public HttpPostedFileBase ImageFile2 { get; set; }
        public HttpPostedFileBase ImageFile3 { get; set; }
    }
}