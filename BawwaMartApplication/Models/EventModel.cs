﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace BawwaMartApplication.Models
{
    public class EventModel
    {
        [Required]
        public int Id { get; set; }
        
        [Required]
        public string Description { get; set; }
        [Required]
        public String Title { get; set; }

        [Required]
        public HttpPostedFileBase ImageFile { get; set; }
    }
}